package tutorial1;

public class Demo {
	public static void main(String args[]){
		push();
		pull();
		checkConflict();
	}
	
	private static void push(){
		System.out.println("Push project...");
	}
	
	
	private static void pull(){
		System.out.println("Poll project...");
	}
	
	
	private static void checkConflict(){
		System.out.println("Check conflict...");
	}
	
}
